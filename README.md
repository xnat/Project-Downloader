# XNAT Project downloader

New XNAT project downloader, that does not use a java applet and allows users to download all data for a project.

![](https://www.dropbox.com/s/z7ql6xula9a94ps/XnatProjectDownloader.png?dl=0&raw=1)

**STATUS:** Its working!

## Installation:

1. Build .zip (manually or via script)

    Make sure the structure of the zip archive is like this:
    ```
        root
        |----src
              |----java
              |     |----org
              |           |----nrg
              |                 |----xnat
              |                        |----restlet
              |                                 |----extensions
              |                                             |----ProjectDownloader.java (not compiled!)
              |----main
                    |----xnat-templates
                                |----screens
                                        |----xnat_projectData
                                                    |----actionsBox
                                                            |----DownloadRecursive.vm
    ```

2. Move .zip to your xnat module directory

        mv project-downloader.zip /opt/xnat_root/modules

3. Update and redeploy XNAT