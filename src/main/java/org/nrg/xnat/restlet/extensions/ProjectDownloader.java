package org.nrg.xnat.restlet.extensions;

/**
 * Created by drakulix on 14.01.15.
 */

import org.nrg.action.ActionException;
import org.nrg.xdat.model.XnatSubjectassessordataI;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectassessordata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.representations.ZipRepresentation;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

@XnatRestlet({"/services/extensions/getprojectfiles", "/services/extensions/getprojectfiles/{PROJECT_ID}"})
public class ProjectDownloader extends SecureResource {
    private static final Logger logger = LoggerFactory.getLogger(ProjectDownloader.class);

    XnatProjectdata proj = null;
    String pID = null;

    public ProjectDownloader(Context context, Request request, Response response) {
        super(context, request, response);
        this.getVariants().add(new Variant(MediaType.ALL));
    }


    @Override
    public boolean allowPut() {
        return false;
    }

    @Override
    public boolean allowPost() {
        return false;
    }

    @Override
    public boolean allowDelete() { return false; }

    @Override
    public Representation represent(Variant variant) {

        pID = (String) getRequest().getAttributes().get("PROJECT_ID");
        if (pID != null) {
            proj = XnatProjectdata.getProjectByIDorAlias(pID, user, false);
        }

        if (proj == null) {
            this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST, "Project ID does not exist");
            return new StringRepresentation("The Project ID specified does not exist.");
        }

        try {
            if (!user.canRead(proj)) {
                this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN, "No access rights on this project");
                return new StringRepresentation("You do not have the required permissions to download these files.");
            }
        } catch (Exception e) {
            this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN, "No access rights on this project");
            return new StringRepresentation("You do not have the required permissions to download these files.");
        }

        MediaType mt = this.getRequestedMediaType();

        if (mt != null && mt != MediaType.APPLICATION_ZIP) {
            this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST, "Wrong MediaType " + mt.toString() + " in Request.");
            return new StringRepresentation("The Data can not be served in the requested format.");
        }

        ZipRepresentation representation;

        try {
            String aRootPath = proj.getArchiveRootPath();
            representation = new ZipRepresentation(MediaType.APPLICATION_ZIP, aRootPath.substring(0, aRootPath.length()-1), identifyCompression(null));
        } catch (ActionException e) {
            this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
            return new StringRepresentation("Error creating the download archive.");
        }

        Set<File> files = new HashSet<File>();

        for (ResourceFile file : proj.getFileResources(proj.getRootArchivePath(), true)) {

            files.add(file.getF());
        }

        for (XnatSubjectdata subj : proj.getParticipants_participant()) {

            try {
                if (!user.canRead(subj)) {
                    continue;
                }
            } catch (Exception e) {
                continue;
            }

            for (ResourceFile file : subj.getFileResources(subj.getArchiveDirectoryName(), true)) {
                files.add(file.getF());
            }

            for (XnatSubjectassessordataI subjai : subj.getExperiments_experiment()) {

                if (subjai instanceof XnatSubjectassessordata) {

                    XnatSubjectassessordata subja = (XnatSubjectassessordata) subjai;

                    try {
                        if (!user.canRead(subja)) {
                            continue;
                        }
                    } catch (Exception e) {
                        continue;
                    }

                    for (ResourceFile file : subja.getFileResources(subja.getArchiveDirectoryName(), true)) {
                        files.add(file.getF());
                    }

                    XnatExperimentdata exp = subja.getExperimentdata();

                    try {
                        if (!user.canRead(exp)) {
                            continue;
                        }
                    } catch (Exception e) {
                        continue;
                    }

                    for (ResourceFile file : exp.getFileResources(exp.getArchiveDirectoryName(), true)) {
                        files.add(file.getF());
                    }
                }
            }
        }

        if (files.isEmpty()) {
            this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN, "No access rights on any subdata. Zip file would be empty.");
            return new StringRepresentation("You do not have the required permissions to download these files.");
        }

        this.setContentDisposition(String.format("%s.zip", pID));

        for (File file : files) {
            String aRootPath = proj.getArchiveRootPath();
            String path = file.getAbsolutePath().replace('\\','/').replace(aRootPath.substring(0, aRootPath.length()-1), "");
            representation.addEntry(path, file);
        }

        return representation;
    }
}
